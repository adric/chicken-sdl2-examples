
default:
	@echo "Available examples:"
	@echo "  make eggsweeper"
	@echo "  make mandelbrot"

clean:
	rm -f eggsweeper/eggsweeper mandelbrot/mandelbrot

.PHONY: eggsweeper mandelbrot

eggsweeper: eggsweeper/eggsweeper
	cd eggsweeper && ./eggsweeper

eggsweeper/eggsweeper: eggsweeper/eggsweeper.scm
	csc -O3 eggsweeper/eggsweeper.scm

mandelbrot: mandelbrot/mandelbrot
	cd mandelbrot && ./mandelbrot

mandelbrot/mandelbrot: mandelbrot/mandelbrot.scm
	csc -O3 mandelbrot/mandelbrot.scm
