;;; This is an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/jcroisant/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This demo program is a clone of Minesweeper, but with eggs instead
;;; of mines (because it's CHICKEN themed).
;;;
;;; This program demonstrates the following concepts:
;;;
;;; - Loading an image file using sdl2-image.
;;; - Converting surface pixel format.
;;; - Setting surface RLE (run length encoding) acceleration.
;;; - Setting surface color key.
;;; - Setting surface blend mode.
;;; - Setting surface color mod.
;;; - Blitting part of a surface (sprite sheet).
;;; - Drawing a grid of tiles composed from multiple image layers.
;;; - Tracking and redrawing "dirty" screen regions.
;;; - Changing the window size and title.
;;; - Handling mouse and keyboard input events.
;;; - Converting mouse coordinates to grid coordinates.
;;;
;;; Controls:
;;;
;;; - Left mouse button (LMB) opens the clicked-on tile.
;;; - Right mouse button (RMB) or Ctrl+LMB toggles flag on the
;;;   clicked-on tile.
;;; - Arrow keys or keypad numbers move the focused tile (in case you
;;;   prefer using the keyboard instead of the mouse).
;;; - Return or keypad enter key opens the focused tile.
;;; - Space or keypad 0 key toggles flag on the focused tile.
;;; - R key restarts the game with the same level.
;;; - Number row keys 1, 2, 3, and 4 change the level (grid size and
;;;   number of eggs).
;;; - Escape key or clicking the close button quits the game.
;;;
;;; Author: John Croisant
;;;
;;; The font for the numbers is "Fipps" by pheist.
;;; http://www.dafont.com/fipps.font


(use (prefix sdl2 sdl2:)
     (prefix sdl2-image img:)
     srfi-1 data-structures vector-lib miscmacros)

(sdl2:init! '(video events))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LEVELS

(define levels
  '((1 . ((width  .   9)
          (height .   9)
          (eggs   .  10)))
    (2 . ((width  .  16)
          (height .  16)
          (eggs   .  40)))
    (3 . ((width  .  30)
          (height .  16)
          (eggs   .  99)))
    (4 . ((width  .  30)
          (height .  20)
          (eggs   . 145)))))


(define *current-level* (void))

(define (select-level! n)
  (set! *current-level* (alist-ref n levels)))

;;; Default to level 2.
(select-level! 2)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; WINDOW

(define window
  (sdl2:create-window! "Eggsweeper"
                       'undefined 'undefined
                       320 320))
(define window-surf (sdl2:window-surface window))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; GAME BOARD / TILES

(define-record-type board
  (make-board width height tiles)
  board?
  (width  board-width)
  (height board-height)
  ;; tiles is a flat vector of tiles on the board, in order from left
  ;; to right, top to bottom.
  (tiles  board-tiles))

(define-record-printer (board b out)
  (display (sprintf "#<board ~Ax~A>"
                    (board-width  b)
                    (board-height b))
           out))


(define-record-type tile
  (make-tile row col open? egg? flag? adj)
  tile?
  (row tile-row)
  (col tile-col)
  (open? tile-open? (setter tile-open?))
  (egg?  tile-egg?  (setter tile-egg?))
  (flag? tile-flag? (setter tile-flag?))
  (adj   tile-adj   (setter tile-adj))) ; number of adjacent eggs

(define-record-printer (tile c out)
  (display (sprintf "#<tile ~A ~A ~A~A~A>"
                    (tile-row c)
                    (tile-col c)
                    (if (tile-open? c) "O" "C")
                    (if (tile-egg?  c) "E" "")
                    (if (tile-flag? c) "F" ""))
           out))


(define (make-board-tiles width height)
  (vector-unfold (lambda (i)
                   (let ((row (modulo i width))
                         (col (inexact->exact (floor (/ i width)))))
                     (make-tile row col #f #f #f 0)))
                 (* width height)))


;;; Call the given procedure once with each tile on the board, in
;;; order from left to right, top to bottom.
(define (board-for-each proc board)
  (vector-for-each (lambda (i tile) (proc tile))
                   (board-tiles board)))

;;; Count the number of tiles on the board for which the predicate
;;; returns a non-#f value.
(define (board-count pred board)
  (vector-count (lambda (i tile) (pred tile))
                (board-tiles board)))

;;; Return a non-#f value if the predicate returns a non-#f value for
;;; every tile on the board.
(define (board-every pred board)
  (vector-every pred (board-tiles board)))


;;; Return the tile at the given board position, or #f if the position
;;; is out of bounds.
(define (board-ref board row col)
  (if (and (< -1 row (board-width board))
           (< -1 col (board-height board)))
      (vector-ref (board-tiles board)
                  (+ row (* col (board-width board))))
      #f))


;;; Return a list of all tiles adjacent to the given board position.
;;; The list may contain 8, 5, or 3 tiles, depending on whether the
;;; position is at the edge or corner of the board.
(define (adjacent-tiles board row col)
  (define offsets '((-1 -1) (0 -1) (1 -1)
                    (-1  0)        (1  0)
                    (-1  1) (0  1) (1  1)))
  (filter identity
          (map (lambda (offset)
                 (let ((row-off (list-ref offset 0))
                       (col-off (list-ref offset 1)))
                   (board-ref board (+ row row-off) (+ col col-off))))
               offsets)))


;;; Returns #t if every tile with an egg is closed, and every tile
;;; without an egg is open. This is the condition for winning the
;;; game.
(define (board-clear? board)
  (board-every (lambda (tile)
                 (or (and (tile-egg? tile)
                          (not (tile-open? tile)))
                     (and (not (tile-egg? tile))
                          (tile-open? tile))))
               board))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BUILDING AND POPULATING BOARD

;;; Make a new board and add eggs according to the level settings.
(define (build-board level)
  (let ((width    (alist-ref 'width  level))
        (height   (alist-ref 'height level))
        (num-eggs (alist-ref 'eggs   level)))
    (populate-board!
     (make-board width height
                 (make-board-tiles width height))
     num-eggs)))


(define (populate-board! board num-eggs)
  (add-eggs! board num-eggs)
  (update-adjacent-egg-counts! board)
  board)


;;; Add eggs to eggless tiles in the board. The eggs will be placed
;;; randomly, unless the board is so full that a random eggless tile
;;; cannot be found within a certain number of tries, in which case
;;; the egg will be placed in the leftmost-topmost eggless tile. If
;;; the board becomes completely full, it will stop attempting to add
;;; any more eggs.
(define (add-eggs! board num-eggs)
  (if (<= num-eggs 0)
      board
      (let ((tile (or (find-random-eggless-tile board)
                      (find-first-eggless-tile board))))
        (if tile
            (begin
              (set! (tile-egg? tile) #t)
              (populate-board! board (sub1 num-eggs)))
            ;; No eggless tiles on the board. End recursion.
            board))))


;;; Randomly check tiles on the board, returning the first tile with
;;; no egg that is found. Returns #f if no eggless tile is found
;;; within the maximum number of tries.
(define (find-random-eggless-tile board #!optional (max-tries 200))
  (let/cc return
    (do ((tiles (board-tiles board))
         (num-tiles (vector-length (board-tiles board)))
         (try 0 (add1 try)))
        ((= try max-tries)
         #f)
      (let ((tile (vector-ref tiles (random num-tiles))))
        (when (not (tile-egg? tile))
          (return tile))))))


;;; Scan the board left-to-right, top-to-bottom for the first tile
;;; with no egg. This is used as a fallback in case a random eggless
;;; tile cannot be found. Returns #f if every tile has an egg.
(define (find-first-eggless-tile board)
  (let ((i (vector-index (compose not tile-egg?)
                         (board-tiles board))))
    (and i (vector-ref (board-tiles board) i))))


(define (update-adjacent-egg-counts! board)
  (board-for-each (lambda (tile)
                    (set! (tile-adj tile)
                          (count tile-egg?
                                 (adjacent-tiles
                                  board (tile-row tile) (tile-col tile)))))
                  board))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COORDINATE SPACE CONVERSION

;;; The size (in pixels) of each sprite in the sprite sheet, and thus
;;; the size of each tile on the board.
(define-constant grid-size 20)


;;; Make an sdl2:rect for the given row and column in the grid.
(define (make-grid-rect row column)
  (sdl2:make-rect (* grid-size row)
                  (* grid-size column)
                  grid-size
                  grid-size))


;;; Returns an sdl2:rect in screen space (pixels) for the given tile.
(define (tile->screen-rect tile)
  (make-grid-rect (tile-row tile) (tile-col tile)))


;;; Return the tile on the board that intersects with the given point
;;; in screen space (pixels). Returns #f if the coordinates are out of
;;; bounds for the board.
(define (screen-point->tile screen-x screen-y board)
  (board-ref board
             (inexact->exact (floor (/ screen-x grid-size)))
             (inexact->exact (floor (/ screen-y grid-size)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPRITES

;;; A surface containing all the sprites for eggsweeper, laid out in a
;;; grid. It is loaded from a PNG file, then converted to the window
;;; surface so it can have a color key.
(define sprite-sheet
  (sdl2:convert-surface (img:load "eggsweeper-sprites.png")
                        (sdl2:surface-format window-surf)))

;; Set a color key (transparent backrgound). The color of the top left
;; pixel (coordinate 0 0) is used as the color key.
(set! (sdl2:surface-color-key sprite-sheet)
      (sdl2:surface-ref-raw sprite-sheet 0 0))


;;; A hash-table holding the row and column of each sprite within the
;;; sprite sheet.
(define sprite-slots
  (alist->hash-table
   '((0            . (0 0))             ; unused
     (1            . (1 0))
     (2            . (2 0))
     (3            . (3 0))
     (4            . (4 0))
     (5            . (5 0))
     (6            . (6 0))
     (7            . (7 0))
     (8            . (8 1))
     (9            . (9 1))             ; unused
     (closed-tile  . (0 1))
     (darker-tile  . (1 1))             ; unused
     (pressed-tile . (2 1))             ; unused
     (open-tile    . (3 1))
     (fail-tile    . (3 1))             ; same as open-tile
     (flag         . (6 1))
     (miss         . (7 1))
     (whole-egg    . (8 1))
     (broken-egg   . (9 1)))))


;;; A hash-table holding color mods for each sprite. The color mod is
;;; an RGB value list that the sprite is "tinted" (multiplied) with.
;;; (255 255 255) means no tint.
(define sprite-color-mods
  (alist->hash-table
   '((1            . (100 170 255))
     (2            . (120 255 120))
     (3            . (240  90  90))
     (4            . (200  90 255))
     (5            . (255 255 120))
     (6            . (120 255 255))
     (7            . (180 180 180))
     (8            . (150  90 200))
     (closed-tile  . (130 210 100))
     (open-tile    . (150 135  90))
     (fail-tile    . (200   0  70))
     (flag         . (255 140   0))
     (miss         . (200   0   0)))))


(define (draw-sprite! sprite-name dest-surf dest-rect)
  (set! (sdl2:surface-color-mod sprite-sheet)
        (hash-table-ref/default sprite-color-mods
                                sprite-name
                                '(255 255 255)))
  (sdl2:blit-surface! sprite-sheet
                      (apply make-grid-rect
                        (hash-table-ref sprite-slots sprite-name))
                      dest-surf
                      dest-rect))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; BAKED TILE SPRITES

;;; Since there are a limited number of possible tile states, we can
;;; make drawing tiles more efficient by "baking" all the possible
;;; tile states (blitting the layers together into a flat image).

(define (baked-tile sprites)
  ;; Create a surface with the same pixel format as the window
  ;; surface, for faster blitting to the window.
  (let ((surface (sdl2:convert-surface
                  (sdl2:make-surface grid-size grid-size 24)
                  (sdl2:surface-format window-surf)))
        (dest-rect (make-grid-rect 0 0)))
    (for-each (lambda (sprite)
                (draw-sprite! sprite surface dest-rect))
              sprites)
    ;; Set RLE (run length encoding) acceleration, for faster blitting.
    (sdl2:surface-rle-set! surface #t)
    ;; All baked tiles are fully opaque, so we might as well disable
    ;; blending to make blitting slightly faster.
    (set! (sdl2:surface-blend-mode surface) 'none)
    surface))


(define baked-tiles
  (alist->hash-table
   `(;; Open tile with an egg -- user broke the egg!
     (broken-egg     . ,(baked-tile '(fail-tile broken-egg)))
     ;; Game over, so reveal that the user flagged an eggless tile.
     (incorrect-flag . ,(baked-tile '(closed-tile whole-egg miss)))
     ;; Game over, so reveal unflagged eggs.
     (unflagged-egg  . ,(baked-tile '(closed-tile whole-egg)))
     ;; Closed tile with a flag.
     (closed-flagged . ,(baked-tile '(closed-tile flag)))
     ;; Closed tile with no flag.
     (closed         . ,(baked-tile '(closed-tile)))
     ;; Open tiles with no egg. Show number of adjacent eggs (except
     ;; for 0, which is blank).
     (0              . ,(baked-tile '(open-tile)))
     (1              . ,(baked-tile '(open-tile 1)))
     (2              . ,(baked-tile '(open-tile 2)))
     (3              . ,(baked-tile '(open-tile 3)))
     (4              . ,(baked-tile '(open-tile 4)))
     (5              . ,(baked-tile '(open-tile 5)))
     (6              . ,(baked-tile '(open-tile 6)))
     (7              . ,(baked-tile '(open-tile 7)))
     (8              . ,(baked-tile '(open-tile 8))))))


(define (tile-state tile game-over?)
  (cond
   ((and (tile-open? tile) (tile-egg? tile))
    'broken-egg)
   ((and game-over? (not (tile-egg? tile)) (tile-flag? tile))
    'incorrect-flag)
   ((and game-over? (tile-egg? tile) (not (tile-flag? tile)))
    'unflagged-egg)
   ((tile-open? tile)
    ;; Tile state is just the number of adjacent eggs.
    (tile-adj tile))
   ((tile-flag? tile)
    'closed-flagged)
   (else
    'closed)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TILE DRAWING

;;; A surface used for highlighting the focused tile. It has an
;;; additive blend mode so it lightens the dest surface when blitted.
(define highlight-surf (sdl2:make-surface grid-size grid-size 32))
(sdl2:fill-rect! highlight-surf #f (sdl2:make-color 60 60 0))
(set! (sdl2:surface-blend-mode highlight-surf) 'add)
(sdl2:surface-rle-set! highlight-surf #t)


(define (draw-tile! tile dest-surf game-over? focused?)
  (let ((dest-rect (tile->screen-rect tile)))
    ;; Blit the baked tile matching the current tile state.
    (sdl2:blit-surface! (hash-table-ref
                         baked-tiles (tile-state tile game-over?))
                        #f dest-surf dest-rect)
    ;; Highlight the tile if it has focus and it's not game over.
    (when (and focused? (not game-over?))
      (sdl2:blit-surface! highlight-surf #f dest-surf dest-rect))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DIRTY TILE MANAGEMENT

;;; "Dirty" tiles are tiles that need to be redrawn, because their
;;; state has changed or they gained or lost focus. This is more
;;; efficient than redrawing all tiles.

(define *dirty-tiles* (make-queue))

(define (any-dirty-tiles?)
  (not (queue-empty? *dirty-tiles*)))

(define (mark-tile-dirty! tile)
  (queue-add! *dirty-tiles* tile))

(define (mark-all-tiles-dirty! board)
  (set! *dirty-tiles*
        (list->queue (vector->list (board-tiles board)))))

(define (redraw-dirty-tiles! dest-surf game-over? focused-tile)
  (while (any-dirty-tiles?)
    (let ((tile (queue-remove! *dirty-tiles*)))
      (draw-tile! tile dest-surf game-over?
                  (eq? tile focused-tile)))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; USER INTERACTION

;;; The current board.
(define *board* #f)


;;; Records whether the game is over. Can be #f (still playing), 'win
;;; (won the game), or 'lose (lost the game).
(define *game-over* #f)

(define (game-won!)
  (set! *game-over* 'win)
  (mark-all-tiles-dirty! *board*)
  (update-window-title!))

(define (game-lost!)
  (set! *game-over* 'lose)
  (mark-all-tiles-dirty! *board*)
  (update-window-title!))


;;; The tile that is currently focused on (because of mouse hover or
;;; arrow keys).
(define *focused-tile* #f)

(define (focus-on-tile! tile)
  (when (not (eq? tile *focused-tile*))
    (when *focused-tile*
      (mark-tile-dirty! *focused-tile*))
    (mark-tile-dirty! tile)
    (set! *focused-tile* tile)))


(define (restart!)
  (set! *board* (build-board *current-level*))
  (set! *game-over* #f)
  (set! *focused-tile* #f)
  (set! *dirty-tiles* (make-queue))
  (resize-window-to-fit!)
  (mark-all-tiles-dirty! *board*)
  (update-window-title!))


;;; Resize the window to fit the current board size.
(define (resize-window-to-fit!)
  (set! (sdl2:window-size window)
        (list (* grid-size (board-width  *board*))
              (* grid-size (board-height *board*))))
  (set! window-surf (sdl2:window-surface window)))


;;; Update the window title to reflect the current game state.
(define (update-window-title!)
  (set! (sdl2:window-title window)
        (sprintf "~A [~A]"
                 (case *game-over*
                   ((win)  "YOU WIN")
                   ((lose) "GAME OVER")
                   (else   "Eggsweeper"))
                 (remaining-flags *board*))))


;;; Return the number of eggs minus the number of flags. (It doesn't
;;; matter whether the flags are correct or not.)
(define (remaining-flags board)
  (- (board-count tile-egg? board)
     (board-count tile-flag? board)))


;;; Attempt to move the focus in the requested direction. dx and dy
;;; are -1, 0, or 1, indicating the number of tiles to move on each
;;; axis. If there is no current focus, set the focus to tile (0 0).
;;; If there is no tile in the requested direction (e.g. at the edge
;;; of the board), the focus does not move. Returns #t if the focus
;;; moved.
(define (try-move-focus! dx dy)
  (if *focused-tile*
      (let ((new-focus (board-ref *board*
                                  (+ dx (tile-row *focused-tile*))
                                  (+ dy (tile-col *focused-tile*)))))
        (when new-focus
          (focus-on-tile! new-focus)))
      (focus-on-tile! (board-ref *board* 0 0))))


;;; Attempt to open the given tile (and possibly adjacent tiles).
(define (try-open-tile! tile)
  (when (not (or (tile-flag? tile) (tile-open? tile)))
    (set! (tile-open? tile) #t)
    (mark-tile-dirty! tile)

    ;; If the tile had an egg, the player loses the game!
    (when (tile-egg? tile)
      (game-lost!))

    ;; If the tile did not have an egg, and there are no adjacent
    ;; eggs, recursively try opening adjacent tiles.
    (if (and (not (tile-egg? tile))
             (= 0 (tile-adj tile)))
        (for-each (lambda (adj-tile)
                    (try-open-tile! adj-tile))
                  (adjacent-tiles
                   *board* (tile-row tile) (tile-col tile))))

    ;; If the board is now clear, the player wins the game!
    (when (board-clear? *board*)
      (game-won!))))


;;; Attempt to toggle flag on the given tile.
(define (try-flag-tile! tile)
  (when (not (tile-open? tile))
    (set! (tile-flag? tile) (not (tile-flag? tile)))
    (mark-tile-dirty! tile)
    (update-window-title!)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN LOOP

(define (main-loop)
  ;; An event struct that will be overwritten by sdl2:wait-event!.
  ;; Passing an existing event struct to sdl2:wait-event! is optional,
  ;; but it reduces the amount of garbage, because sdl2:wait-event!
  ;; allocates a new event if none is passed. Since there may be many
  ;; events handled per second, that small bit of garbage can add up.
  (define event (sdl2:make-event))

  (restart!)

  ;; Create a continuation that can be called to exit the main loop.
  (let/cc exit-main-loop!
    ;; Loop forever (until exit-main-loop! is called).
    (while #t
      (when (any-dirty-tiles?)
        (redraw-dirty-tiles! window-surf *game-over* *focused-tile*)
        (sdl2:update-window-surface! window))

      ;; Wait for the next event, then handle it.
      (handle-event (sdl2:wait-event! event) exit-main-loop!))))


;;; Disable various irrelevant event types, to avoid wasted time and
;;; memory garbage from handling them.
(set! (sdl2:event-state 'text-editing) #f)
(set! (sdl2:event-state 'text-input) #f)
(set! (sdl2:event-state 'mouse-wheel) #f)
(set! (sdl2:event-state 'finger-down) #f)
(set! (sdl2:event-state 'finger-up) #f)
(set! (sdl2:event-state 'finger-motion) #f)
(set! (sdl2:event-state 'multi-gesture) #f)


(define (handle-event ev exit-main-loop!)
  (case (sdl2:event-type ev)
    ;; Window exposed, etc.
    ((window)
     (sdl2:update-window-surface! window))

    ;; User requested app quit (e.g. clicked the close button).
    ((quit)
     (exit-main-loop! #t))

    ;; Keyboard key pressed
    ((key-down)
     (case (sdl2:keyboard-event-sym ev)
       ;; Escape quits the program
       ((escape)
        (exit-main-loop! #t))

       ;; R restarts the game with the current difficulty level
       ((r)
        (restart!))

       ;; Number row keys switch difficulty level
       ((n-1)  (select-level! 1) (restart!))
       ((n-2)  (select-level! 2) (restart!))
       ((n-3)  (select-level! 3) (restart!))
       ((n-4)  (select-level! 4) (restart!))

       ;; Arrow keys or keypad numbers try to move focus
       ((kp-1)         (try-move-focus! -1  1))
       ((kp-2   down)  (try-move-focus!  0  1))
       ((kp-3)         (try-move-focus!  1  1))
       ((kp-4   left)  (try-move-focus! -1  0))
       ((kp-6  right)  (try-move-focus!  1  0))
       ((kp-7)         (try-move-focus! -1 -1))
       ((kp-8     up)  (try-move-focus!  0 -1))
       ((kp-9)         (try-move-focus!  1 -1))

       ;; Return or keypad enter tries to open focused tile
       ((return kp-enter)
        (when (and *focused-tile* (not *game-over*))
          (try-open-tile! *focused-tile*)))

       ;; Space or keypad 0 tries to toggle flag focused tile
       ((space kp-0)
        (when (and *focused-tile* (not *game-over*))
          (try-flag-tile! *focused-tile*)))))

    ;; Mouse button pressed
    ((mouse-button-down)
     (let* ((mouse-x (sdl2:mouse-button-event-x ev))
            (mouse-y (sdl2:mouse-button-event-y ev))
            (tile (screen-point->tile mouse-x mouse-y *board*)))
       (case (sdl2:mouse-button-event-button ev)
         ;; If Ctrl is not being held, left mouse button tries to open
         ;; the tile. But if Ctrl is being held, left mouse button
         ;; tries to toggle flag, acting like right mouse button.
         ((left)
          (when (and tile (not *game-over*))
            (if (not (member 'ctrl (sdl2:mod-state)))
                (try-open-tile! tile)
                (try-flag-tile! tile))))

         ;; Right mouse button tries to toggle flag on clicked tile
         ((right)
          (when (and tile (not *game-over*))
            (try-flag-tile! tile))))))

    ;; Mouse cursor moved
    ((mouse-motion)
     (let* ((mouse-x (sdl2:mouse-motion-event-x ev))
            (mouse-y (sdl2:mouse-motion-event-y ev))
            (tile (screen-point->tile mouse-x mouse-y *board*)))
       ;; Focus on the tile that the mouse is pointing at.
       (when (not *game-over*)
         (focus-on-tile! tile))))))


(main-loop)
