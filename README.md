# chicken-sdl2 examples

This repository contains example games and programs built with
[chicken-sdl2](https://gitlab.com/chicken-sdl2/chicken-sdl2),
CHICKEN Scheme bindings to Simple DirectMedia Layer 2 (SDL2).

Unless otherwise noted, all code and assets in this repository are
provided under the
[CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](http://creativecommons.org/publicdomain/zero/1.0/).


## Eggsweeper

![Screenshot of Eggsweeper](eggsweeper/screenshot.png)

```
make eggsweeper
```

A clone of Minesweeper.

The font for the numbers is
["Fipps" by pheist](http://www.dafont.com/fipps.font).

### Controls

- Left mouse button (LMB) opens the clicked-on tile.
- Right mouse button (RMB) or Ctrl+LMB toggles flag on the
  clicked-on tile.
- Arrow keys or keypad numbers move the focused tile (in case you
  prefer using the keyboard instead of the mouse).
- Return or keypad enter key opens the focused tile.
- Space or keypad 0 key toggles flag on the focused tile.
- R key restarts the game with the same level.
- Number row keys 1, 2, 3, and 4 change the level (grid size and
  number of eggs).
- Escape key or clicking the close button quits the game.


## Mandelbrot

![Screenshot of Mandelbrot](mandelbrot/screenshot.jpg)

```
make mandelbrot
```

An interactive
[Mandelbrot set](https://en.wikipedia.org/wiki/Mandelbrot_set)
explorer program.

### Controls

- Arrow keys move the view around.
  - Hold the shift key to move in larger increments.
  - Hold the alt key to move in smaller increments.
- Plus (+) or equals (=) zooms in.
- Minus (-) zooms out.
- R resets to the original view.
- Number row keys change the color palette (not all numbers have a
  palette).
- S saves a screenshot.
  - The screenshot will be placed in the current directory, named
    like "mandelbrot-_____.bmp", with an integer timestamp.
- Escape or clicking the close button quits the app.
