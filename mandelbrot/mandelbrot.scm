;;; This is an example program demonstrating chicken-sdl2:
;;; https://gitlab.com/jcroisant/chicken-sdl2-examples
;;;
;;; The contents of this file are made available under the CC0 1.0
;;; Universal Public Domain Dedication. See LICENSE-CC0.txt or visit
;;; http://creativecommons.org/publicdomain/zero/1.0/


;;; This program renders the Mandelbrot set, a popular fractal image.
;;; The user can move around and zoom in and out to explore the
;;; fractal, and change the color palette.
;;;
;;; This program demonstrates the following concepts:
;;;
;;; - Setting individual pixels of a surface
;;; - Using and modifying surface palettes
;;; - Scaled surface blits
;;; - Saving surfaces as BMP images
;;; - A main loop handling rendering jobs and user input
;;; - Using CHICKEN type declarations
;;;
;;; Controls:
;;;
;;; - Arrow keys move the view around.
;;;   - Hold the shift key to move in larger increments.
;;;   - Hold the alt key to move in smaller increments.
;;; - Plus (+) or equals (=) zooms in.
;;; - Minus (-) zooms out.
;;; - R resets to the original view.
;;; - Number row keys change the color palette (not all numbers have a
;;;   palette).
;;; - S saves a screenshot.
;;;   - The screenshot will be placed in the current directory, named
;;;     like "mandelbrot-_____.bmp", with an integer timestamp.
;;; - Escape or clicking the close button quits the app.
;;;
;;; Author: John Croisant


(use (prefix sdl2 sdl2:)
     data-structures miscmacros)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; INITIALIZATION

(sdl2:init! '(video events))

(define-constant max-iterations 255)

(define-constant window-w 840)
(define-constant window-h 700)

(define window (sdl2:create-window!
                "Mandelbrot"
                'centered 'centered
                window-w window-h))
(define window-surf (sdl2:window-surface window))

;;; The main surface upon which the image will be drawn, and then
;;; blitted to the window. It is an 8-bit surface, so it will have a
;;; palette with 256 colors.
(define main-surf (sdl2:make-surface window-w window-h 8))

;;; The palette for main-surf (and preview).
(define main-palette (sdl2:surface-palette main-surf))

;;; A small surface (1/100th scale) for rendering a quick preview.
(define preview
  (sdl2:make-surface
   (inexact->exact (floor (/ window-w 10)))
   (inexact->exact (floor (/ window-h 10)))
   8))

;;; Share palette between preview and main-surf.
(set! (sdl2:surface-palette preview) main-palette)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; UTIL

(declare (inline rect-right))
(define (rect-right rect)
  (+ (sdl2:rect-x rect) (sdl2:rect-w rect)))

(declare (inline rect-bottom))
(define (rect-bottom rect)
  (+ (sdl2:rect-y rect) (sdl2:rect-h rect)))

;;; Return a new sdl2:rect the same size as the given surface.
(declare (inline surf-rect))
(define (surf-rect surf)
  (sdl2:make-rect 0 0 (sdl2:surface-w surf) (sdl2:surface-h surf)))

(declare (inline sq))
(: sq (number -> number))
(define (sq n)
  (* n n))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MANDELBROT SET

;;; Escape time algorithm for the Mandelbrot set. Returns how many
;;; iterations it took for the given point to escape, an integer from
;;; 1 to max-iterations. The number of iterations is then mapped onto
;;; the color palette to determine the color to draw at that point.
(: mandelbrot-iterations (float float -> fixnum))
(define (mandelbrot-iterations x0 y0)
  (if (or (in-cardioid? x0 y0) (in-bulb2? x0 y0))
      max-iterations
      (do ((i 0 (add1 i))
           (x 0.0)
           (y 0.0)
           (temp 0.0))
          ((or (= i max-iterations) (< 4 (+ (sq x) (sq y))))
           i)
        (set! temp (+ x0 (- (sq x) (sq y))))
        (set! y (+ y0 (* 2 x y)))
        (set! x temp))))


;;; Returns #t if the coordinates are within the main cardioid (center
;;; heart shape). This is used as a quick check to avoid unnecessary
;;; computation.
(: in-cardioid? (float float -> boolean))
(define (in-cardioid? x y)
  (let ((q (+ (sq (- x 1/4)) (sq y))))
    (< (* q (+ q (- x 1/4)))
       (* 1/4 (sq y)))))

;;; Returns #t if the coordinates are within the period-2 bulb (left
;;; circle shape). This is used as a quick check to avoid unnecessary
;;; computation.
(: in-bulb2? (float float -> boolean))
(define (in-bulb2? x y)
  (< (+ (sq (+ x 1)) (sq y))
     1/16))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; COORDINATE SPACE CONVERSION

(define default-zoom 1)
(define min-zoom 1)
(define max-zoom (expt 2 16))

(define default-focus-x -0.75)
(define default-focus-y 0)

(define *zoom* default-zoom)
(define *focus-x* default-focus-x)
(define *focus-y* default-focus-y)

(define (reset-view!)
  (set! *zoom*    default-zoom)
  (set! *focus-x* default-focus-x)
  (set! *focus-y* default-focus-y))


;;; Convert a point from surface space (pixels) to real space
;;; (coordinates that can be passed to the Mandelbrot algorithm).
(: surf-space->real-space
   (fixnum fixnum fixnum fixnum -> (list float float)))
(define (surf-space->real-space surf-x surf-y surf-w surf-h)
  (let ((x-norm (- (/ (+ surf-x 0.5) surf-w) 0.5))
        (y-norm (- (/ (+ surf-y 0.5) surf-h) 0.5)))
    (list (+ *focus-x* (/ (* 3.0 x-norm) *zoom*))
          (+ *focus-y* (/ (* 2.5 y-norm) *zoom*)))))


;;; Double the zoom level, unless already zoomed in the maximum
;;; allowed amount. Returns #t if the zoom level changed, or #f if it
;;; did not change.
(define (zoom-in!)
  (if (>= *zoom* max-zoom)
      #f
      (begin
        (set! *zoom* (* *zoom* 2))
        #t)))

;;; Halve the zoom level, unless already zoomed out the minimum
;;; allowed amount. Returns #t if the zoom level changed, or #f if it
;;; did not change.
(define (zoom-out!)
  (if (<= *zoom* min-zoom)
      #f
      (begin
        (set! *zoom* (/ *zoom* 2))
        #t)))

;;; Move the focus in the given direction. dx and dy are -1, 0, or 1,
;;; indicating the direction of movement. The magnitude of movement is
;;; affected by the current zoom level, and whether the shift and/or
;;; alt keys are being held. If shift is held, the movement is 5 times
;;; as far. If alt is held, the movement is 1/10 as far. The keys are
;;; cumulative, so if both are held, the movement is 5/10 as far.
(define (move! dx dy #!optional shift? alt?)
  (let ((multiplier (* 0.15
                       (/ 1 *zoom*)
                       (if shift?   5 1)
                       (if alt?   0.1 1))))
    (inc! *focus-x* (* dx multiplier))
    (inc! *focus-y* (* dy multiplier))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RENDERING

(define *render-jobs* (make-queue))

(define (cancel-pending-render-jobs!)
  (set! *render-jobs* (make-queue)))

(define (render-job-pending?)
  (not (queue-empty? *render-jobs*)))

(define (add-render-job! . args)
  (queue-add! *render-jobs* args))


;;; Take the next render job off the job queue, perform the rendering,
;;; and blit the result to the screen.
(define (do-next-render-job!)
  (let* ((job (queue-remove! *render-jobs*))
         (surf (list-ref job 0))
         (region (list-ref job 1)))
    (render-surf-region! surf region)
    (sdl2:blit-surface! surf region window-surf region)
    (sdl2:update-window-surface! window)))


;;; Generate and enqueue many render jobs, to sequentially render
;;; regions of the given surface. Each render job renders a square
;;; region of the given size.
(define (generate-render-jobs! surf #!key (size 50))
  (do ((y 0 (+ y size)))
      ((>= y (sdl2:surface-h surf)))
    (do ((x 0 (+ x size)))
        ((>= x (sdl2:surface-w surf)))
      (let ((w (min size (- (sdl2:surface-w surf) x)))
            (h (min size (- (sdl2:surface-h surf) y))))
        (add-render-job! surf (sdl2:make-rect x y w h))))))


;;; Render one rectangular region of the given surface. The region is
;;; an sdl2:rect (in surface pixel coordinates) specifying which part
;;; of the surface to render.
(define (render-surf-region! surf region)
  (let ((surf-w (sdl2:surface-w surf))
        (surf-h (sdl2:surface-h surf)))
    (do ((surf-y (sdl2:rect-y region) (add1 surf-y)))
        ((= surf-y (rect-bottom region)))
      (do ((surf-x (sdl2:rect-x region) (add1 surf-x)))
          ((= surf-x (rect-right region)))
        (let ((real-xy (surf-space->real-space
                        surf-x surf-y surf-w surf-h)))
          (sdl2:surface-set!
           surf surf-x surf-y
           (mandelbrot-iterations (car real-xy) (cadr real-xy))))))))



;;; Render a low-resolution preview and scaled-blit it to the screen.
;;; This gives the user quick feedback about what they will see if
;;; they allow the screen to fully render.
(define (render-preview!)
  ;; Render the mandelbrot set onto the preview surface. The preview
  ;; surface is small, so we just draw it all at once, rather than
  ;; generating jobs.
  (render-surf-region! preview (surf-rect preview))

  ;; Convert the preview surface to the same format as the window
  ;; surface so that we can do a scaled blit.
  (let* ((format (sdl2:surface-format window-surf))
         (converted (sdl2:convert-surface preview format)))

    ;; Fill the screen with gray and make the converted preview
    ;; partially transparent so that the preview will appear somewhat
    ;; duller than the final image.
    (sdl2:fill-rect! window-surf #f (sdl2:make-color 128 128 128))
    (set! (sdl2:surface-alpha-mod converted) 128)
    (set! (sdl2:surface-blend-mode converted) 'blend)

    ;; Scaled-blit the converted preview to the window.
    (sdl2:blit-scaled! converted #f window-surf #f)
    (sdl2:update-window-surface! window)))


;;; Initiate a full re-rendering of the screen. This happens whenever
;;; the focus or zoom changes (but not when the palette changes).
(define (full-rerender!)
  (cancel-pending-render-jobs!)
  (render-preview!)
  (sdl2:fill-rect! main-surf #f 255)
  (generate-render-jobs! main-surf size: 50))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PALETTES


;;; Converts HSL color values to a list of RGB color values. All color
;;; values here are floats in the range 0.0 to 1.0.
(: hsl->rgb (float float float -> (list float float float)))
(define (hsl->rgb h s l)
  (define (hsl->rgb-helper p q t)
    (when (< t 0) (set! t (add1 t)))
    (when (> t 1) (set! t (sub1 t)))
    (cond ((< t 1/6)  (+ p (* (- q p) 6 t)))
          ((< t 1/2)  q)
          ((< t 2/3)  (+ p (* (- q p) (- 2/3 t) 6)))
          (else       p)))
  (if (= s 0)
      (list l l l)
      (let* ((q (if (< l 0.5)
                    (* l (+ 1 s))
                    (+ l (- s (* l s)))))
             (p (- (* 2 l) q)))
        (list (hsl->rgb-helper p q (+ h 1/3))
              (hsl->rgb-helper p q h)
              (hsl->rgb-helper p q (- h 1/3))))))


;;; Like hsl->rgb, but returns an sdl2:color.
(define (hsl->sdl2-color h s l)
  (apply sdl2:make-color
    (map (lambda (x) (floor (* x 255)))
         (hsl->rgb h s l))))


(define black (sdl2:make-color 0 0 0))


;;; A trippy palette that starts out with sunset-like colors around
;;; the outsides, but shifts towards greens and yellows on the inside.
(define (trippy-palette-fn i ncolors)
  (if (= i ncolors)
      black
      (sdl2:make-color (* 8 (modulo i 32))
                      i
                      (- 128 (* 2 (modulo i 64))))))


;;; A vibrant palette that cycles through the whole rainbow several
;;; times, with gradually increasing luminosity.
(define (rainbow-1-palette-fn i ncolors)
  (if (= i ncolors)
      black
      (hsl->sdl2-color
       (/ (modulo (* 4 (+ i 40)) ncolors)
          ncolors)
       0.8
       (+ 0.3 (* 0.3 (/ i ncolors))))))


;;; A weird palette using prime numbers so that it never repeats the
;;; same color. Based on a palette found online.
(define (weird-palette-fn i ncolors)
  (if (= i ncolors)
      black
      (sdl2:make-color (* 5  (modulo i 15))
                      (* 32 (modulo i 7))
                      (* 8  (modulo i 31)))))

;;; A cycling grayscale palette that goes from black to white in 32
;;; steps, then repeats.
(define (black-white-32-palette-fn i ncolors)
  (if (= i ncolors)
      black
      (let ((n (* 8 (modulo i 32))))
        (sdl2:make-color n n n))))

;;; A cycling grayscale palette that goes from white to black in 32
;;; steps, then repeats.
(define (white-black-32-palette-fn i ncolors)
  (if (= i ncolors)
      (sdl2:make-color 255 255 255)
      (let ((n (- 255 (* 8 (modulo i 32)))))
        (sdl2:make-color n n n))))

;;; A grayscale palette that simply goes from black to white. The
;;; brightness of each pixel directly corresponds to the number of
;;; iterations.
(define (raw-palette-fn i ncolors)
  (sdl2:make-color i i i))


;;; Build a vector of sdl2:colors by repeatedly calling the given
;;; palette function with increasing i value.
(define (build-colors palette-fn #!optional (ncolors 256))
  (do ((colors (make-vector ncolors))
       (i 0 (add1 i)))
      ((= i ncolors)
       colors)
    (vector-set! colors i (palette-fn i (sub1 ncolors)))))

;;; A list of color vectors for all the built-in palettes, in order by
;;; numerical slot (for when the user presses a number key to switch
;;; palette). The first slot corresponds to the 1 key, the last slot
;;; corresponds to the 0 key. Slots with no palette yet are set to #f.
(define palette-color-vectors
  (list (build-colors trippy-palette-fn)
        (build-colors rainbow-1-palette-fn)
        (build-colors weird-palette-fn)
        (build-colors black-white-32-palette-fn)
        (build-colors white-black-32-palette-fn)
        #f
        #f
        #f
        #f
        (build-colors raw-palette-fn)))


(define *current-palette* -1)

;;; Attempt to switch to palette number n, if it exists. Returns #t if
;;; the palette changed, or #f if the palette did not change (because
;;; the user selected the current palette again, or the requested
;;; palette did not exist).
(define (switch-palette! n)
  (let ((colors (list-ref palette-color-vectors n)))
    (if (and colors (not (= n *current-palette*)))
        (begin
          (sdl2:palette-colors-set! main-palette colors)
          (set! *current-palette* n)
          #t)
        #f)))

(switch-palette! 0)


;;; Blit the entire main surface to the window and update the window.
;;; This occurs after a palette change, to show the new colors.
(define (refresh!)
  (sdl2:blit-surface! main-surf #f window-surf #f)
  (sdl2:update-window-surface! window))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MAIN LOOP

(define (save-screenshot!)
  (let ((filename (sprintf "mandelbrot-~S.bmp"
                           (inexact->exact (current-seconds)))))
    (if (= 0 (sdl2:save-bmp! window-surf filename))
        (printf "Saved screenshot: ~A~N~!" filename)
        (printf "Screenshot failed: ~A~N~!" (sdl2:get-error)))))


(define (main-loop)
  (full-rerender!)

  ;; Create a continuation that can be called to exit the main loop.
  (let/cc exit-main-loop!
    ;; Loop forever (until exit-main-loop! is called).
    (while #t
      ;; Handle all pending events.
      (let ((ev (sdl2:poll-event!)))
        (while ev
          (handle-event ev exit-main-loop!)
          (set! ev (sdl2:poll-event!))))

      ;; If there are any render jobs pending, do the next job.
      ;; Otherwise, just stop and wait for a new event to occur.
      (if (render-job-pending?)
          (do-next-render-job!)
          (handle-event (sdl2:wait-event!) exit-main-loop!)))))


(define (handle-event ev exit-main-loop!)
  (case (sdl2:event-type ev)
    ;; Window exposed, resized, etc.
    ((window)
     (sdl2:update-window-surface! window))

    ;; User requested app quit (e.g. clicked the close button).
    ((quit)
     (exit-main-loop! #t))

    ;; Keyboard key pressed
    ((key-down)
     (let ((shift? (memq 'shift (sdl2:keyboard-event-mod ev)))
           (alt?   (memq 'alt (sdl2:keyboard-event-mod ev))))
       (case (sdl2:keyboard-event-sym ev)
         ;; Plus or Equals zooms in
         ((plus equals)
          (when (zoom-in!)
            (full-rerender!)))

         ;; Minus zooms out
         ((minus)
          (when (zoom-out!)
            (full-rerender!)))

         ;; Arrow keys move the view
         ((up)
          (move! 0 -1 shift? alt?)
          (full-rerender!))
         ((down)
          (move! 0 1 shift? alt?)
          (full-rerender!))
         ((left)
          (move! -1 0 shift? alt?)
          (full-rerender!))
         ((right)
          (move! 1 0 shift? alt?)
          (full-rerender!))

         ;; Number row keys switch palettes
         ((n-1)  (when (switch-palette! 0) (refresh!)))
         ((n-2)  (when (switch-palette! 1) (refresh!)))
         ((n-3)  (when (switch-palette! 2) (refresh!)))
         ((n-4)  (when (switch-palette! 3) (refresh!)))
         ((n-5)  (when (switch-palette! 4) (refresh!)))
         ((n-6)  (when (switch-palette! 5) (refresh!)))
         ((n-7)  (when (switch-palette! 6) (refresh!)))
         ((n-8)  (when (switch-palette! 7) (refresh!)))
         ((n-9)  (when (switch-palette! 8) (refresh!)))
         ((n-0)  (when (switch-palette! 9) (refresh!)))

         ;; R resets to the original view
         ((r)
          (reset-view!)
          (full-rerender!))

         ;; S saves a screenshot, tagged with the current time in
         ;; seconds since 1970-01-01T00:00Z.
         ((s)
          (save-screenshot!))

         ;; Escape quits the program
         ((escape)
          (exit-main-loop! #t)))))))


(main-loop)
